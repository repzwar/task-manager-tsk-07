package ru.pisarev.tm;

import ru.pisarev.tm.constant.ArgumentConst;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.model.Command;
import ru.pisarev.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (runArgs(args)) exit();
        process();
    }

    private static boolean runArgs (final String[] args){
        if (args == null || args.length == 0)  return false;
        run(args[0]);
        return true;
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
            case ArgumentConst.ARG_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
            case ArgumentConst.ARG_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
            case ArgumentConst.ARG_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_INFO:
            case ArgumentConst.ARG_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.INFO);
        System.out.println(Command.HELP);
        System.out.println(Command.EXIT);
    }

    private static void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    private static void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    private static void displayVersion() {
        System.out.println("0.6.0");
    }

    private static void displayAbout() {
        System.out.println("Aleksey Pisarev");
        System.out.println("pisarevaleks@mail.ru");
    }

    private static void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void exit() {
        System.exit(0);
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            displayWait();
            command = scanner.nextLine();
            run(command);
        }
    }

}
